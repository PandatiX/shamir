#include <stdio.h>
#include <assert.h>
#include <gmp.h>
#include <stdlib.h>

/* https://fr.wikipedia.org/wiki/Partage_de_cl%C3%A9_secr%C3%A8te_de_Shamir */
/* https://www.geeksforgeeks.org/shamirs-secret-sharing-algorithm-cryptography/ */

const unsigned SIZEOF_MPZ_T = sizeof(mpz_t);

typedef struct {
    unsigned x;
    mpz_t y;
} MPZPoint;

typedef struct {
    unsigned size;
    MPZPoint *arr;
} PointsArray;

typedef struct {
    unsigned size;
    mpz_t ***arr;
} OptimisedDifArray;

OptimisedDifArray newOptimisedDifArray(unsigned size) {
    unsigned i, j, iS, jS;
    OptimisedDifArray o;

    assert(size != 0);

    o.size = size;
    iS = size - 1;
    o.arr = (mpz_t***) malloc(iS * sizeof(mpz_t*));
    for (i = 0; i < iS; i++) {
        jS = i + 1;
        o.arr[i] = (mpz_t**) malloc(jS * SIZEOF_MPZ_T);
        for (j = 0; j < jS; j++) {
            o.arr[i][j] = (mpz_t*) malloc(SIZEOF_MPZ_T);
            mpz_init(*o.arr[i][j]);
        }
    }
    return o;
}

void freeOptimizedDifArray(OptimisedDifArray o) {
    unsigned i, j;
    for (i = 0; i < o.size - 1; i++) {
        for (j = 0; j < i + 1; j++) {
            mpz_clear(*o.arr[i][j]);
            free(o.arr[i][j]);
        }
        free(o.arr[i]);
    }
    free(o.arr);
}

void setCoefOptimizedDifArray(OptimisedDifArray o, mpz_t t, unsigned i, unsigned j) {
    assert(i != 0 && j != 0 && i != j);
    assert(i <= o.size && j <= o.size);

    if (i > j) {
        mpz_set(*o.arr[i-2][j-1], t);
    } else {
        mpz_ui_sub(*o.arr[i-1][j-2], 0, t);
    }
}

mpz_t* getCoefOptimizedDifArray(OptimisedDifArray o, unsigned i, unsigned j) {
    mpz_t *tmp;

    assert(i != 0 && j != 0 && i != j);
    assert(i <= o.size && j <= o.size);

    if (i > j) {
        return o.arr[i-2][j-1];
    } else {
        tmp = (mpz_t*) malloc(SIZEOF_MPZ_T);
        mpz_init_set_ui(*tmp, 0);
        mpz_sub(*tmp, *tmp, *o.arr[i - 1][j - 2]);
        return tmp;
    }
}

PointsArray newPointsArray(unsigned size) {
    unsigned i;
    PointsArray p;
    p.size = size;
    p.arr = (MPZPoint*) malloc(size * sizeof(MPZPoint));
    for (i = 0; i < size; i++) {
        mpz_init(p.arr[i].y);
    }
    return p;
}

void freePointsArray(PointsArray p) {
    unsigned i;
    for (i = 0; i < p.size; i++) {
        mpz_clear(p.arr[i].y);
    }
    free(p.arr);
}

/* TODO scalability -> can "only" share the secret with 2.147.483.647 * 2 (= 4.294.967.294) members */
PointsArray getShamirPoints(unsigned bits, unsigned k, unsigned n, mpz_t secret) {
    unsigned nNum = k - 1, i, x;
    gmp_randstate_t rstate;
    mpz_t *numbers, tmp;
    PointsArray pts;

    /* Assert preliminary conditions */
    assert(k > 1 && n > 1 && k <= n);

    gmp_randinit_mt(rstate);

    /* Initialize numbers */
    numbers = (mpz_t*) malloc(nNum * SIZEOF_MPZ_T);
    for (i = 0; i < nNum; i++) {
        mpz_init(numbers[i]);
        mpz_urandomb(numbers[i], rstate, bits);
    }

    /* Create points */
    mpz_init(tmp);

    /* Compute f(x) */
    pts = newPointsArray(n);
    for (x = 0; x < n; x++) {
        pts.arr[x].x = x + 1;
        mpz_init_set(pts.arr[x].y, secret);             /* points[x] = secret */
        for (i = 0; i < nNum; i++) {
            mpz_set_ui(tmp, x+1);                       /* x+1 */
            mpz_pow_ui(tmp, tmp, i+1);                  /* (x+1)^(i+1) */
            mpz_mul(tmp, tmp, numbers[i]);              /* tmp = numbers[i] * (x+1)^(i+1) */
            mpz_add(pts.arr[x].y, pts.arr[x].y, tmp);   /* y += tmp */
        }
    }

    mpz_clear(tmp);
    for (i = 0; i < nNum; i++) {
        mpz_clear(numbers[i]);
    }
    gmp_randclear(rstate);
    free(numbers);

    return pts;
}

mpz_t* getShamirSecret(PointsArray keys) {
    mpz_t *secret, li;
    unsigned i, j;

    /* Assert preliminary conditions */
    assert(keys.size > 1);
    assert(keys.arr != NULL);

    secret = malloc(SIZEOF_MPZ_T);
    mpz_init_set_ui(*secret, 0);
    mpz_init(li);

    for (i = 0; i < keys.size; i++) {

        /* Initializing the fraction */
        mpz_set(li, keys.arr[i].y);
        for (j = 0;  j < keys.size; j++) {

            /* Computing the Lagrange terms */
            if (i != j) {
                mpz_mul_ui(li, li, -keys.arr[j].x);                 /* li *= -x[j]     */
                mpz_div_ui(li, li, keys.arr[i].x-keys.arr[j].x);    /* li /= x[i]-x[j] */
            }
        }
        mpz_add(*secret, *secret, li);          /* secret += li */
    }

    mpz_clear(li);

    return secret;
}

mpz_t* improvedGetShamirSecret(PointsArray keys) {
    unsigned i, j, k;
    mpz_t *secret, frontTerm, tmp, den;
    OptimisedDifArray o;

    /* Assert preliminary conditions */
    assert(keys.size > 1);
    assert(keys.arr != NULL);

    /* Compute front term */
    mpz_init_set_d(frontTerm, (keys.size % 2 == 0) ? 1 : -1);
    printf("Front term = ");
    mpz_out_str(stdout, 10, frontTerm);
    printf("\n");

    /* Compute OptimizedDifArray */
    mpz_init(tmp);
    o = newOptimisedDifArray(keys.size);

    for (i = 1; i <= keys.size; i++) {
        for (j = 1; j <= keys.size; j++) {
            if (i != j) {
                mpz_set_d(tmp, keys.arr[i].x - keys.arr[j].x);
                setCoefOptimizedDifArray(o, tmp, i, j);
            }
        }
    }

    /* Compute secret */
    secret = (mpz_t*) malloc(SIZEOF_MPZ_T);
    mpz_init_set_ui(*secret, 0);
    mpz_init_set_ui(den, 1);

    for (i = 0; i < keys.size; i++) {
        mpz_set(tmp, keys.arr[i].y);
        for (j = 0; j < keys.size; j++) {
            if (i != j) {
                mpz_mul_ui(tmp, tmp, keys.arr[j].x);
                for (k = 0; k < keys.size; k++) {
                    if (j != k) {
                        mpz_mul(tmp, tmp, *getCoefOptimizedDifArray(o, j + 1, k + 1));
                    }
                }

                printf("[%d;%d] Den = ", i, j);
                mpz_out_str(stdout, 10, den);
                printf("\n");
                mpz_mul(den, den, *getCoefOptimizedDifArray(o, i + 1, j + 1));
            }
        }
        mpz_add(*secret, *secret, tmp);
    }
    printf("Num = ");
    mpz_out_str(stdout, 10, *secret);
    printf("\nDen = ");
    mpz_out_str(stdout, 10, den);

    mpz_div(*secret, *secret, den);

    printf("\nComputed secret = ");
    mpz_out_str(stdout, 10, *secret);
    printf("\n");

    return secret;
}

int main() {
    /*
    printf ("Enter your number: ");
    scanf("%1023s" , inputStr);

    //Parse the input string as a base 10 number
    flag = mpz_set_str(n, inputStr, 10);
    assert (flag == 0);
    */

    const unsigned K = 3;
    const unsigned N = 5;
    const unsigned S = 605176371;

    PointsArray keys;
    mpz_t *s;
    unsigned i;

    mpz_t secret;
    mpz_init_set_ui(secret, S);

    keys = getShamirPoints(2048, K, N, secret);
    for (i = 0; i < keys.size; i++) {
        printf("[%d] key: ", i);
        mpz_out_str(stdout, 10, keys.arr[i].y);
        printf("\n");
    }

    keys.size = K;
    /*s = improvedGetShamirSecret(keys);*/
    s = getShamirSecret(keys);
    printf("Secret: ");
    mpz_out_str(stdout, 10, *s);
    printf(" ; Real secret: ");
    mpz_out_str(stdout, 10, secret);
    printf("\n");

    freePointsArray(keys);
    mpz_clear(*s);
    free(s);
    mpz_clear(secret);

    return 0;
}
